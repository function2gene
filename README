INSTALLATION 

Prerequisites

Function2Gene requires the following modules and programs to work
properly:

  * perl http://www.perl.org/get.html
  * HTML::TreeBuilder http://search.cpan.org/dist/HTML-Tree/
  * HTML::ElementTable http://search.cpan.org/dist/HTML-Element-Extended/
  * XML::Parser http://search.cpan.org/dist/XML-Parser/

The operating system-specific instructions below will help you to
install these modules.

Unix instructions

If you're running a reasonably modern Linux (or other UNIX variant,
like MacOS X) you already have perl and wget. If so, you can install
the remaining modules by doing the following:

 cpan install HTML::TreeBuilder
 cpan install HTML::Element::Extended
 cpan install XML::Parser

If you are running Debian, you can just run the following:

 aptitude install wget perl libhtml-element-extended-perl libxml-parser-perl

Windows

If you are using a windows-based machine, first consider switching to
(or dual-booting) Debian or Ubuntu or a similar Free Software
distribution. If switching isn't for you, you will need to install
cygwin (http://www.cygwin.com/) (a linux-like environment for
windows), which (by default) will install perl and wget.

You can then use the cpan commands listed below to install the required
modules (and any needed depependencies):

 cpan install HTML::TreeBuilder
 cpan install HTML::Element::Extended
 cpan install XML::Parser

Other Operating Systems

If you are using another operating system (minux, MacOS 9, UNIVAX,
Plan9, OS/2, etc.) you'll need to refer to your operating system's
documentation for information on how to install the required modules.


USAGE

All of the programs are located within the bin directory of the tarball
which is downloadable above. First, change directories to the bin
directory (cd bin.)

Then create a kewyords file which contains kewyords separated by
newlines. You can optionally add a weight for each keyword separated by
a tab. (That is, the file will contain lines of
'keyword<tab>0.3<newline>', for example.) You can do this in any text
editor that is capable of saving text files, like vim or emacs.

Finally, make a results directory (mkdir keyword_results), and run
function2gene with the --keywords option specifying the keywords file
that you have created and the --results option specifying the results
directory you have created (./function2gene --keywords keywords.txt
--results keyword_results.)

The results will be in the keyword_results directory in a file called
combined_results.txt, and a sumary table will be there as well, called
combined_results_table.txt

The details of the operation of function2gene are explained in its
documentation; type ./function2gene --man; to see it.

As a demonstration, you can simply do the following, which creates a
keywords.txt file with a single keyword, 'transferrin', and runs the
keywords:

  cd bin;
  echo 'transferrin' > keywords.txt
  mkdir keyword_results;
  ./function2gene --keywords keywords.txt --results keyword_results;

which creates a keywords file with a single keyword, transferrin, and
runs the various subscripts, putting the results in a keyword_results
directory.

